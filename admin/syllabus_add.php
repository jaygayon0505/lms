<?php 

    include('../session.php');
    require("../opener_db.php");

    $errmsg_arr = array();
    $errflag = false;
    $conn = $connector->DbConnector();
    $res = null;

    if ($_FILES['uploaded_file']['size'] >= 1048576 * 5) {
        $errmsg_arr[] = 'file selected exceeds 5MB size limit';
        $errflag = true;
    }

    $rd2 = mt_rand(1000, 9999) . "_syllabus";

    if ((!empty($_FILES["uploaded_file"])) && ($_FILES['uploaded_file']['error'] == 0) && (!$errflag)) {
        $filename = basename($_FILES['uploaded_file']['name']);
        $ext = substr($filename, strrpos($filename, '.') + 1);
        
        if (($ext != "exe") && ($_FILES["uploaded_file"]["type"] != "application/x-msdownload")) {
            $newname = "uploads/" . $rd2 . "_" . $filename;
            $name_notification  = 'Add Downloadable Syllabus.';
            if (!file_exists($newname)) {
                if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $newname))) {
                    $fbatch = $_POST['fbatch'];
                    $fyrlvl = $_POST['fyrlvl'];
                    $teacher_id = $_POST['teacher_select'];
                    $res = mysqli_query($conn,"INSERT INTO `file_syllabus` (`file_id`, `file_loc`, `fdate_in`, `fbatch`, `fyrlvl`, `teacher_id`) VALUES (NULL, '$newname', NOW(), '$fbatch', '$fyrlvl', $teacher_id);");  
                }
            }
        }
    }

    echo $res; 

?>