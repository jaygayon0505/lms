### Credentials ###

*username: augusto@solutionsresource.com
*password: passcode_1

### Working Modules (Expected) ###

* Login
* Register
* Update and Viewing of User Details (Photo not included due to backend issue)
* POP Submission
* Camera Module in OCR and BARCODE Scan (No API)

Note: Some of our services were currently unavailable, we are expecting that some of our modules are also not working properly.