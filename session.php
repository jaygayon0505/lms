<?php
//Start session
session_start();
$conn = mysqli_connect('localhost','root','','capstone') or die(mysqli_error($conn));

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['id']) || ($_SESSION['id'] == '')) {
    header("location: index.php");
    exit();
}

$session_id=$_SESSION['id'];
?>