   <div class="row-fluid">
	<!-- block -->
	<div class="block">
		<div class="navbar navbar-inner block-header">
			<div class="muted pull-left">Add Student</div>
		</div>
		<div class="block-content collapse in">
			<div class="span12">
			<form id="add_student" method="post">
			
					<div class="control-group">
						<div class="controls">
						<select  name="class_id" class="" required>
							<option></option>
						<?php
						$cys_query = mysqli_query($conn,"select * from class order by class_name");
						while($cys_row = mysqli_fetch_array($cys_query)){
						
						?>
						<option value="<?php echo $cys_row['class_id']; ?>"><?php echo $cys_row['class_name']; ?></option>
						<?php } ?>
						</select>
						</div>
					</div>
			
					<div class="control-group">
						<div class="controls">
						<input name="un" class="input focused" id="focusedInput" type="text" placeholder = "ID Number / Username" required>
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls">
						<input name="fn" class="input focused" id="focusedInput" type="text" placeholder = "Firstname" required>
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls">
						<input  name="ln" class="input focused" id="focusedInput" type="text" placeholder = "Lastname" required>
						</div>
					</div>

					<div class="input-group">
						<div class="controls">
						<button class="btn btn-outline-secondary" type="button" id="generate_password">Generate Password</button>   
						<input type="text" name="password" class="form-control generate_password_text" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
						</div>
					</div>


					<div class="control-group">
						<div class="controls">
							<button name="save" class="btn btn-info"><i class="icon-plus-sign icon-large"></i></button>
						</div>
					</div>
			</form>
			</div>
		</div>
	</div>
	<!-- /block -->
</div>
					
<script type="text/javascript">
	$(document).ready( function() {
	let r = "stnd_"+Math.random().toString(36).substring(3);
	$('.generate_password_text').val(r);

	$('#generate_password').click( function() {
	let r = "stnd_"+Math.random().toString(36).substring(3);
	$('.generate_password_text').val(r);
		});				
	});

	jQuery(document).ready(function($){
		$("#add_student").submit(function(e){
			e.preventDefault();
			var _this = $(e.target);
			var formData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "save_student.php",
				data: formData,
				success: function(html){
					if(html == 1){
						$.jGrowl("Student Successfully  Added", { header: 'Student Added' });
						$('#studentTableDiv').load('student_table.php', function(response){
							$("#studentTableDiv").html(response);
							$('#example').dataTable( {
								"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
								"sPaginationType": "bootstrap",
								"oLanguage": {
									"sLengthMenu": "_MENU_ records per page"
								}
							} );
							$(_this).find(":input").val('');
							$(_this).find('select option').attr('selected',false);
							$(_this).find('select option:first').attr('selected',true);
							location.reload();
						});
					}
				}
			});
		});
	});
</script>		
