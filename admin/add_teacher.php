<div class="row-fluid">
<!-- block -->
  <div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">Add Teacher</div>
    </div>
    <div class="block-content collapse in">
      <div class="span12">

        <form method="post">

          <div class="control-group">
            <label>Department:</label>
            <div class="controls">
              <select name="department" value="<?php echo $row['department_id']; ?>" class="">
                <?php
                $query = mysqli_query($conn,"select * from department order by department_name");
                while($row = mysqli_fetch_array($query)){

                ?>
                <option selected="selected" value="<?php echo $row['department_id']; ?>"><?php echo $row['department_name']; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="control-group">
            <div class="controls">
              <input class="input focused" name="firstname" id="focusedInput" type="text" placeholder = "Firstname">
            </div>
          </div>

          <div class="control-group">
            <div class="controls">
              <input class="input focused" name="lastname" id="focusedInput" type="text" placeholder = "Lastname">
            </div>
          </div>

          <div class="control-group">
            <div class="controls">
              <input class="input focused" name="email" id="focusedInput" type="text" placeholder = "Username/Email">
            </div>
          </div>

          <div class="input-group">
            <div class="controls">
              <button class="btn btn-outline-secondary" type="button" id="generate_password">Generate Password</button>   
              <input type="text" name="password" class="form-control generate_password_text" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
            </div>
          </div>

          <div class="control-group">
            <div class="controls">
              <button name="save" class="btn btn-info"><i class="icon-plus-sign icon-large"></i></button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
<!-- /block -->
</div>


<script type="text/javascript">
	$(document).ready( function() {
    let r = "tchr_"+Math.random().toString(36).substring(3);
    $('.generate_password_text').val(r);

		$('#generate_password').click( function() {
      let r = "tchr_"+Math.random().toString(36).substring(3);
      $('.generate_password_text').val(r);
		});				
	});
</script>

<?php
if (isset($_POST['save'])) {
//   foreach ($_POST as $key => $value) {
//     echo "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";
// }
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$department_id = $_POST['department'];
$email = $_POST['email'];
$password = $_POST['password'];

$check_val = $firstname == "" || $lastname == "" || $email == "" || $password == "" ;

$query = mysqli_query($conn,"select * from teacher where firstname = '$firstname' and lastname = '$lastname' ")or die(mysqli_error($conn));
$count = mysqli_num_rows($query);

if ($count > 0){ ?>
  <script>
    alert('Data Already Exist');
  </script>
<?php
} else if($check_val) {
  echo "Please complete all the details above.";
} else{
  
  mysqli_query($conn,"insert into teacher (firstname,lastname,location,department_id,username,password,about,teacher_status,teacher_stat)
  values ('$firstname','$lastname','uploads/NO-IMAGE-AVAILABLE.jpg','$department_id', '$email', '$password','','','');         
  ") or die(mysqli_error($conn)); 
  ?>
  <script>
    window.location = "teachers.php"; 
  </script>
<?php   }} ?>
						 
						 