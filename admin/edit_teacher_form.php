<div class="row-fluid">
<a href="teachers.php" class="btn btn-info"><i class="icon-plus-sign icon-large"></i> Add Teacher</a>
	<!-- block -->
	<div class="block">
	<div class="navbar navbar-inner block-header">
	<div class="muted pull-left">Edit Teacher</div>
	</div>
	<div class="block-content collapse in">
	<div class="span12">
		<form method="post">

			<?php
			$query = mysqli_query($conn,"select * from teacher where teacher_id = '$get_id' ")or die(mysqli_error($conn));
			$row = mysqli_fetch_array($query);
			?>

			<div class="control-group">
				<label>Department:</label>
				<div class="controls">
				<select name="department" value="<?php echo $row['department_id']; ?>" class="">
					<?php
					$query1 = mysqli_query($conn,"select * from department order by department_name");
					while($row1 = mysqli_fetch_array($query1)){
					?>
					<option selected="selected" value="<?php echo $row1['department_id']; ?>"><?php echo $row1['department_name']; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>

			<div class="control-group">
			<div class="controls">
			<input class="input focused" value="<?php echo $row['firstname']; ?>" name="firstname" id="focusedInput" type="text" placeholder = "Firstname">
			</div>
			</div>

			<div class="control-group">
			<div class="controls">
			<input class="input focused" value="<?php echo $row['lastname']; ?>"  name="lastname" id="focusedInput" type="text" placeholder = "Lastname">
			</div>
			</div>

			<div class="control-group">
				<div class="controls">
				<input class="input focused" value="<?php echo $row['username']; ?>" name="email" id="focusedInput" type="text" placeholder = "Username/Email">
				</div>
			</div>

			<div class="input-group">
				<div class="controls">
				<button class="btn btn-outline-secondary" type="button" id="generate_password_tchr">Generate Password</button>   
				<input type="text" value="<?php echo $row['password']; ?>" name="password" class="form-control generate_password_text_tchr" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
				</div>
			</div>			

			<div class="control-group">
			<div class="controls">
			<button name="update" class="btn btn-success"><i class="icon-save icon-large"></i></button>
			</div>
			</div>

		</form>
	</div>
	</div>
	</div>
	<!-- /block -->
</div>

<script type="text/javascript">
	$('#generate_password_tchr').click( function() {
		let r = "tchr_"+Math.random().toString(36).substring(3);
		$('.generate_password_text_tchr').val(r);			
	});
</script>				
					
<?php
if (isset($_POST['update'])) {

	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$department_id = $_POST['department'];
	$email = $_POST['email'];
	$password = $_POST['password'];

	$check_val = $firstname == "" || $lastname == "" || $email == "" || $password == "" ;
	
	$query = mysqli_query($conn,"select * from teacher where firstname = '$firstname' and lastname = '$lastname' ")or die(mysqli_error($conn));
	$count = mysqli_num_rows($query);
	
	if ($count > 1){ ?>
		<script>
		alert('Data Already Exist');
		</script>
	<?php }else if($check_val) {
		echo "Please complete all the details above.";
	} else {
		mysqli_query($conn,"update teacher set firstname = '$firstname', username = '$email', password = '$password', lastname = '$lastname' , department_id = '$department_id' where teacher_id = '$get_id' ")or die(mysqli_error($conn)); ?>
		<script>
		window.location = "teachers.php"; 
		</script>
<?php }} ?>
						 
						 