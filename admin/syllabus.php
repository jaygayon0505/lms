<?php include('session.php'); ?>
<?php include('header.php'); ?>
    <body>
        <?php include('navbar.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php include('syllabus_sidebar.php'); ?>
                <!-- Start -->
                <div class="span3" id="">
                    <div class="row-fluid">
                        <div id="block_bg" class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Upload Syllabus</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                <form class="" id="add_syllabus" method="post" enctype="multipart/form-data" name="upload" >
                                        <div class="control-group">
                                            <label class="control-label" for="inputEmail">File:</label>
                                            <div class="controls">
                                                <input name="uploaded_file"  class="input-file uniform_on" id="fileInput" type="file" required>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                                                <input type="hidden" name="id" value="<?php echo $session_id ?>"/>
                                            </div>
                                        </div>
                                        <div class="controls">
                                            <select name="teacher_select"  class="" required>
                                                <option></option>
                                                <?php
                                                    $query = mysqli_query($conn,"SELECT * FROM `teacher` ORDER BY `lastname`");
                                                    while($row = mysqli_fetch_array($query)){ ?>
                                                        <option value="<?php echo $row['teacher_id']; ?>"><?php echo $row['lastname'].' '.$row['firstname']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="control-group">
                                            <div class="controls">
                                            <input class="input focused" name="fbatch" id="focusedInput" type="text" placeholder = "Batch" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="controls">
                                            <input class="input focused" name="fyrlvl" id="focusedInput" type="text" placeholder = "Year Level" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="control-group">
                                            <div class="controls">
                                                <button name="Upload" type="submit" value="Upload" class="btn btn-success" /><i class="icon-upload-alt"></i>&nbsp;Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="span6" id="">
                    <div class="row-fluid">
                        <div id="block_bg" class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">List of uploaded Syllabus</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form action="syllabus_delete.php" method="post">
                                        <table cellpadding="0" cellspacing="0" class="table" id="example">
                                            <a data-toggle="modal" href="#syllabus_delete" id="delete"  class="btn btn-danger" name=""><i class="icon-trash icon-large"></i></a>
                                            <?php include('modal_delete.php'); ?>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>ID</th>
                                                    <th>File Name</th>
                                                    <th>Year Level</th>
                                                    <th>Batch</th>
                                                    <th>Teacher</th>
                                                    <th>Timestamp</th>
                                                </tr>  
                                            </thead>
                                            <tbody>
                                            <!-- START TABLE -->
                                            <?php
                                                $file_syllabus = mysqli_query($conn,"SELECT * FROM `file_syllabus` INNER JOIN `teacher` WHERE `file_syllabus`.`teacher_id` = `teacher`.`teacher_id`") or die(mysqli_error($conn));
                                                while($row = mysqli_fetch_array($file_syllabus)){
                                                    $id = $row['file_id']; ?>
                                                <!-- START OF CONTENT -->
                                                <tr>
                                                    <td width="30"><input id="optionsCheckbox" class="uniform_on" name="selector[]" type="checkbox" value="<?php echo $id; ?>"></td>
                                                    <td><?php echo $row['file_id']; ?></td>
                                                    <td><?php echo $row['file_loc']; ?></td>
                                                    <td><?php echo $row['fyrlvl']; ?></td>
                                                    <td><?php echo $row['fbatch']; ?></td>
                                                    <td><?php echo "ID : ".$row['teacher_id']." <br/>".$row['lastname'].", ".$row['firstname']; ?></td>
                                                    <td><?php echo $row['fdate_in']; ?></td>
                                                </tr>
                                                <!-- END OF CONTENT -->
                                            <?php } ?>
                                            <!-- END TABLE -->
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End -->
                <?php include('footer.php'); ?>
            </div>
        </div>
        <?php include('script.php'); ?>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                $("#add_syllabus").submit(function(e){
                    $.jGrowl("Uploading File Please Wait......", { sticky: true });
                    e.preventDefault();
                    var _this = $(e.target);
                    var formData = new FormData($(this)[0]);
                    $.ajax({
                        type: "POST",
                        url: "syllabus_add.php",
                        data: formData,
                        success: function(response){
                            $.jGrowl("Student Successfully  Added", { header: 'Student Added' });
                            setTimeout(function(){
                                window.location = 'syllabus.php';
                            },2000)
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                });
            });
        </script>
    </body>
</html>