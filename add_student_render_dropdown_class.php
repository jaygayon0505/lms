<?php 
    session_start(); 
    $conn = mysqli_connect('localhost','root','','capstone') or die(mysqli_error($conn)); 
    $models = array();
    $query = mysqli_query($conn,"select * from class order by class_name");
    while($row = mysqli_fetch_array($query)) {
        $models[] = array(
            'class_id' => $row['class_id'], 
            'class_name' => $row['class_name']
        );
    }
    echo json_encode($models);
?>