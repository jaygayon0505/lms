<?php include('header_dashboard.php'); ?>
    <body id="class_div">
		<?php include('navbar_about.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12" id="content">
                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
								<div class="navbar navbar-inner block-header">
									<div id="" class="muted pull-right"><a href="index.php"><i class="icon-arrow-left"></i> Back</a></div>
								</div>
                            <div class="block-content collapse in">
							<h3>Developers</h3>
							<hr>
                                <div class="span3">
										<center>
										<img id="developers" src="admin/images/ces.jpg" class="img-circle">
										<hr>
										<p>Name: Frances Heather D. Aballe</p>
										<p>Address: Caloocan City</p>
										<p>Email: heather.aballe@gmail.com</p>
										<p>Position: Programmer</p>
										</center>
								</div>
                                <div class="span3">
															<center>
								<img id="developers" src="admin/images/andrea.jpg" class="img-circle">
								<hr>
																				<p>Name: Andrea Jane Austero</p>

										<p>Address: Caloocan City</p>
										<p>Email: austerojane23@gmail.com</p>
										<p>Position: Project Manager</p>
								</center>
								</div>
                                <div class="span3">
															<center>
								<img id="developers" src="admin/images/chris.jpg" class="img-circle">
								<hr>
												<p>Name: Christian Cimatu</p>
										<p>Address: Caloocan City</p>
										<p>Email: chris.cimatu25@gmail.com</p>
										<p>Position: Designer</p>
								</center>
								</div>
                                <div class="span3">
															<center>
								<img id="developers" src="admin/images/ken.jpg" class="img-circle">
								<hr>
												<p>Name: Kinji Jacar</p>
										<p>Address: Caloocan City</p>
										<p>Email: Kenkenjacar@gmail.com</p>
										<p>Position: Designer</p>

								</center>
								</div>
								<div class="span3">
															<center>
								<img id="developers" src="admin/images/rannie.jpg" class="img-circle">
								<hr>
												<p>Name: Rannie Makiling</p>
										<p>Address: Caloocan City</p>
										<p>Email: rannie.makiling@gmail.com</p>
										<p>Position: Programmer</p>

								</center>
								</div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>
		<?php include('footer.php'); ?>
        </div>
		<?php include('script.php'); ?>
    </body>
</html>