<?php include('session.php'); ?>
<?php include('header_dashboard.php'); ?>
<?php $get_id = $_GET['id']; ?>
    <body>
		<?php include('navbar_teacher.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
				<?php include('class_sidebar.php'); ?>
                <div class="span9" id="content">
                     <div class="row-fluid">
					     <!-- breadcrumb -->
				        <div class="pull-right">
							<a href="my_students.php<?php echo '?id='.$get_id; ?>" class="btn btn-info"><i class="icon-arrow-left"></i> Back</a>
						</div>
						<?php $class_query = mysqli_query($conn,"select * from teacher_class
						LEFT JOIN class ON class.class_id = teacher_class.class_id
						LEFT JOIN subject ON subject.subject_id = teacher_class.subject_id
						where teacher_class_id = '$get_id'")or die(mysqli_error($conn));
						$class_row = mysqli_fetch_array($class_query); ?>
					    <ul class="breadcrumb">
							<li><a href="#"><?php echo $class_row['class_name']; ?></a> <span class="divider">/</span></li>
							<li><a href="#"><?php echo $class_row['subject_code']; ?></a> <span class="divider">/</span></li>
							<li><a href="#">My Students</a><span class="divider">/</span></li>
							<li><a href="#"><b>Add Student</b></a></li>
						</ul>
						<!-- end breadcrumb -->
                        <!-- block -->
                        <div id="block_bg" class="block">
                            <div class="navbar navbar-inner block-header">
                                <div id="" class="muted pull-left"></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12"> 
									<form method="post" action="">
										
										<div style="margin-top:10px;">
											<label>Course</label>
											<select class="form-select" aria-label="Default select example" id="dd_course">
											</select>
										</div>
										<div style="margin-bottom:10px; display:none" id="dd_lbl">
											<label>Year Level</label>
											<select class="form-select" aria-label="Default select example" id="dd_yrlvl" style="display:none">
												<option></option>
												<option value="1">1st</option>
												<option value="2">2nd</option>
												<option value="3">3rd</option>
												<option value="4">4th</option>
											</select>
										</div>
										<button type="button" class="btn btn-info" style="margin-bottom:20px; display:none" id="dd_btn"><i class="icon-save"></i>&nbsp;Search</button> <br/><br/>

										<button name="submit" type="submit" class="btn btn-info" style="margin-bottom:20px;"><i class="icon-save"></i>&nbsp;Add Student</button>

										<input type="hidden" id="get_id" value="<?php echo $get_id; ?>">
										<input type="hidden" id="session_id" value="<?php echo $session_id; ?>">
										<table cellpadding="0" cellspacing="0" class="table" id="example">
											<thead>
												<tr>
													<th>Photo</th>
													<th>Name</th>
													<th>Course Year and Section</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php $a = 0 ;
												$q = "select * from student LEFT JOIN class on class.class_id = student.class_id WHERE student_id = -1";
												if(isset($_GET['dd'])){
													if($_GET['dd'] == "ALL") {
														$q = "select * from student LEFT JOIN class on class.class_id = student.class_id";
													}
												}
												if(isset($_GET['cy'])){
													$q = 'select * from student LEFT JOIN class on class.class_id = student.class_id WHERE `class_name` LIKE "'.$_GET['cy'].'%" ';
												}
												$query = mysqli_query($conn,$q) or die(mysqli_error($conn));
												while ($row = mysqli_fetch_array($query)) {
												$id = $row['student_id'];
												$a++; ?>
												<tr>
													<input type="hidden" name="test" value="<?php echo $a; ?>">
													<td width="70"><img  class="img-rounded" src="admin/<?php echo $row['location']; ?>" height="50" width="40"></td>
													<td><?php echo $row['firstname'] . " " . $row['lastname']; ?></td> 
													<td><?php echo $row['class_name']; ?></td> 
													<td width="80">
														<select name="add_student<?php echo $a; ?>" class="span12">
															<?php
																if(!isset($_GET['cy'])){
																	echo "<option></option>";
																}
															?>
															<option>Add</option>
														</select>
														<input type="hidden" name="student_id<?php echo $a; ?>" value="<?php echo $id; ?>">
														<input type="hidden" name="class_id<?php echo $a; ?>" value="<?php echo $get_id; ?>">
														<input type="hidden" name="teacher_id<?php echo $a; ?>" value="<?php echo $session_id; ?>">
													</td>
													<?php } ?>    
												</tr>
											</tbody>
										</table>
									</form>
									<!-- Start of Submit -->
										<?php if (isset($_POST['submit'])){
											$test = $_POST['test'];
											for($b = 1; $b <=  $test; $b++){	
											$test1 = "student_id".$b;
											$test2 = "class_id".$b;
											$test3 = "teacher_id".$b;
											$test4 = "add_student".$b;
											
											$id = $_POST[$test1];
											$class_id = $_POST[$test2];
											$teacher_id = $_POST[$test3];
											$Add = $_POST[$test4];
											
											$query = mysqli_query($conn,"select * from teacher_class_student where student_id = '$id' and teacher_class_id = '$class_id' ")or die(mysqli_error($conn));
											$count = mysqli_num_rows($query); 
											if ($count > 0) { ?>
												<script>
													alert('Student Already in the class'); 
													window.location = "add_student.php<?php echo '?id='.$get_id; ?>"; 
												</script>
											<?php } else if($Add == 'Add') {
												mysqli_query($conn,"insert into teacher_class_student (student_id,teacher_class_id,teacher_id) values('$id','$class_id','$teacher_id') ")or die(mysqli_error($conn));
											} ?>
												<script>
													window.location = "my_students.php<?php echo '?id='.$get_id; ?>"; 
												</script>
										<?php } } ?>	
									<!-- End of Submit -->				
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>
			<?php include('footer.php'); ?>
        </div>
		<?php include('script.php'); ?>
		<script type="text/javascript">
            jQuery(document).ready(function($){

				$( "#dd_course" ).change(function() {
					if($(this).val() === "CLEAR") {
						$( "#dd_lbl" ).fadeOut("fast");
						$("#dd_btn").fadeOut("fast");
						$( "#dd_yrlvl" ).fadeOut("fast");
						window.location ="add_student.php?id=<?php echo $get_id; ?>&dd=CLEAR";
					} else if($(this).val() === "ALL") {
						$( "#dd_lbl" ).fadeOut("fast");
						$("#dd_btn").fadeOut("fast");
						$( "#dd_yrlvl" ).fadeOut("fast");
						window.location ="add_student.php?id=<?php echo $get_id; ?>&dd=ALL";
					} else {
						$( "#dd_yrlvl" ).fadeIn();
						$( "#dd_lbl" ).fadeIn("fast");
					}
				});

				$( "#dd_yrlvl" ).change(function() {
					if($("#dd_yrlvl").val()>0) $("#dd_btn").fadeIn();
					else  $("#dd_btn").fadeOut();
				});

				$( "#dd_btn" ).click(function() {
					const cy = $( "#dd_course" ).val()+"-"+$("#dd_yrlvl").val();
					window.location ="add_student.php?id=<?php echo $get_id; ?>&cy="+cy;
				});

				function getUnique(arr){
					let uniqueArr = [];
					for(let i of arr) {
						if(uniqueArr.indexOf(i) === -1) {
							uniqueArr.push(i);
						}
					}
					return uniqueArr;
				}

                $.ajax({
					type: "GET",
					url: "add_student_render_dropdown_class.php",
					success: function(response){
						const obj = JSON.parse(response);
						let list = [];
						obj.map((item)=>{
							let data = item.class_name
							list.push(data.split('-')[0]);
						});
						const dropdown = getUnique(list);
						$('#dd_course').append($('<option>', { 
								value: "CLEAR",
								text : "" 
						}));
						$('#dd_course').append($('<option>', { 
								value: "ALL",
								text : "ALL" 
						}));
						dropdown.forEach((item, index)=>{
							$('#dd_course').append($('<option>', { 
								value: item,
								text : item 
							}));

						});
					},
					cache: false,
					contentType: false,
					processData: false
				});
            });
        </script>
    </body>
    
</html>