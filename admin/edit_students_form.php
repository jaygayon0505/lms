<div class="row-fluid">
    <a href="students.php" class="btn btn-info"><i class="icon-plus-sign icon-large"></i> Add Student</a>
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Add Student</div>
        </div>
        <div class="block-content collapse in">

          <?php
          $query = mysqli_query($conn,"select * from student LEFT JOIN class ON class.class_id = student.class_id where student_id = '$get_id'")or die(mysqli_error($conn));
          $row = mysqli_fetch_array($query);
          ?>

          <div class="span12">
            <form method="post">        
              <div class="control-group">
                <div class="controls">
                  <select  name="cys" class="" required>
                    <option value="<?php echo $row['class_id']; ?>"><?php echo $row['class_name']; ?></option>
                    <?php
                    $cys_query = mysqli_query($conn,"select * from class order by class_name");
                    while($cys_row = mysqli_fetch_array($cys_query)){

                    ?>
                    <option value="<?php echo $cys_row['class_id']; ?>"><?php echo $cys_row['class_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

            <div class="control-group">
              <div class="controls">
                <input name="un" value="<?php echo $row['username']; ?>" class="input focused" id="focusedInput" type="text" placeholder = "ID Number" required>
              </div>
            </div>

            <div class="control-group">
              <div class="controls">
                <input name="fn"  value="<?php echo $row['firstname']; ?>"  class="input focused" id="focusedInput" type="text" placeholder = "Firstname" required>
              </div>
            </div>

            <div class="control-group">
              <div class="controls">
                <input  name="ln"  value="<?php echo $row['lastname']; ?>" class="input focused" id="focusedInput" type="text" placeholder = "Lastname" required>
              </div>
            </div>

            <div class="input-group">
              <div class="controls">
                <button class="btn btn-outline-secondary" type="button" id="generate_password_edit">Generate Password</button>   
                <input type="text" value="<?php echo $row['password']; ?>" name="password" class="form-control generate_password_text_edit" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
              </div>
            </div>


              <div class="control-group">
                <div class="controls">
                  <button name="update" class="btn btn-success"><i class="icon-save icon-large"></i></button>
                </div>
              </div>
            </form>
          </div>

        </div>
    </div>
    <!-- /block -->
</div>
					
<script type="text/javascript">
	$(document).ready( function() {
    $('#generate_password_edit').click( function() {
      let r = "stnd_"+Math.random().toString(36).substring(3);
      $('.generate_password_text_edit').val(r);
    });				
  });
</script>					
		
<?php
if (isset($_POST['update'])) {

$un = $_POST['un'];
$fn = $_POST['fn'];
$ln = $_POST['ln'];
$pw = $_POST['password'];
$cys = $_POST['cys'];

mysqli_query($conn,"update student set username = '$un' , firstname ='$fn' , lastname = '$ln', password = '$pw' , class_id = '$cys' where student_id = '$get_id' ")or die(mysqli_error($conn));
?>

<script>
window.location = "students.php"; 
</script>

<?php } ?>
	