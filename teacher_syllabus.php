<?php include('session.php'); ?>
<?php include('header_dashboard.php'); ?>
    <body>
        <?php include('navbar_teacher.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
            <?php include('teacher_syllabus_sidebar.php'); ?>
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div id="block_bg" class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">List of Syllabus</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table cellpadding="0" cellspacing="0" class="table" id="example">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Year Level</th>
                                                <th>Batch</th>
                                                <th>Teacher</th>
                                                <th>Timestamp</th>
                                                <th>Download</th>
                                            </tr>  
                                        </thead>
                                        <tbody>
                                        <!-- START TABLE -->
                                        <?php
                                            $file_syllabus = mysqli_query($conn,"SELECT * FROM `file_syllabus` INNER JOIN `teacher` WHERE `file_syllabus`.`teacher_id` = `teacher`.`teacher_id` AND `file_syllabus`.`teacher_id` = ".$_SESSION['id']." ORDER BY `fbatch` DESC;") or die(mysqli_error($conn));
                                            while($row = mysqli_fetch_array($file_syllabus)){
                                                $id = $row['file_id']; ?>
                                            <!-- START OF CONTENT -->
                                            <tr>
                                                <td><?php echo $row['file_id']; ?></td>
                                                <td><?php echo $row['fyrlvl']; ?></td>
                                                <td><?php echo $row['fbatch']; ?></td>
                                                <td><?php echo "ID : ".$row['teacher_id']." <br/>".$row['lastname'].", ".$row['firstname']; ?></td>
                                                <td><?php echo $row['fdate_in']; ?></td>
                                                <td width="30"><a href="<?php echo "admin/".$row['file_loc']; ?>"><i class="icon-download icon-large"></i></a></td> 
                                            </tr>
                                            <!-- END OF CONTENT -->
                                        <?php } ?>
                                        <!-- END TABLE -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End -->
                <?php include('footer.php'); ?>
            </div>
        </div>
        <?php include('script.php'); ?>
    </body>

</html>